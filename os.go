// +build !linux,!windows,!darwin,!freebsd,!netbsd,!openbsd,!dragonfly,!solaris

package platform

import "runtime"

const (
	OperatingSystem = runtime.GOOS
)
