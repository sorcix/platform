// +build arm

package platform

// User friendly name for the GOARCH variable.
//
// In case this package does not know a better name, it will return the value of GOARCH instead.
const (
	Architecture = "ARM"
)
