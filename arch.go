// +build !386,!amd64,!arm,!arm64

package platform

import "runtime"

// User friendly name for the GOARCH variable.
//
// In case this package does not know a better name, it will return the value of GOARCH instead.
const (
	Architecture = runtime.GOARCH
)
